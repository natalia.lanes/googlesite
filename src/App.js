import React, { Component, Fragment } from 'react';
import Dashboard from './ui/screens/dashboard/dashboard'
import Header from './components/header/header'
import Footer from './components/footer/footer'
import SideBar from './components/sidebar/sidebar'
import HomePage from './ui/screens/home-page/homepage'
import DropDown from './components/dropdown/dropdown'
import EmployeesInformation from './ui/screens/employees-information/employessinformation'
import SuggestAProject from './ui/screens/suggestaproject/suggestaproject'
import MoreInformation from './ui/screens/more-information/moreinformation';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import './App.css'

class App extends Component {

  constructor() {
    super()
    this.state = {
      selected: false
    }
  }

  selecionado = () => {
    this.setState({ selected: !this.state.selected })
  }

  render() {
    return (
      <Fragment>
        {/* <BrowserRouter> */}
        <Switch>
          <Route exact={true} path="/" component={MoreInformation} />
          <Route path="/informacoes" component={EmployeesInformation} />
          <Route path="/sugerir-novo-projeto" component={SuggestAProject} />
          {/* <Route path="/maiores-informacoes" component={MoreInformation} /> */}
        </Switch>
        {/* </BrowserRouter> */}
        <Header onClick={this.selecionado}></Header>
        <SideBar selected={this.state.selected}></SideBar>
        {/* <Dashboard></Dashboard> */}
        {/* <EmployeesInformation></EmployeesInformation> */}
        <Footer></Footer>
      </Fragment>
    );
  }
}

export default App;
