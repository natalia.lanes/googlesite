import React, { Component } from 'react';
import './customcard.css'
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import bolsistas from '../../ui/assets/images/bolsistas.svg'
import { Link } from 'react-router-dom'

export default class CustomCard extends Component {

    constructor() {
        super()
        this.state = {
            isLiked: false
        }
    }

    like = () => {
        this.setState({ isLiked: !this.state.isLiked })
    }

    render() {
        return (
            <Card className="card">
                <CardMedia className="card-image"
                    image={bolsistas}
                    title="Paella dish"
                />
                <CardContent className="card-summary">
                    <Typography component="p" className="card-title">
                        <span>
                            {this.props.projectName}
                        </span>
                    </Typography>
                    <Typography component="p" className="card-summary">
                        <span>{this.props.projectDescription}</span>
                    </Typography>
                </CardContent>
                <CardActions disableActionSpacing className="card-buttons">
                    {/* <IconButton aria-label="Add to favorites" onClick={this.like}>
                        <FavoriteIcon className={this.state.isLiked ? "card-icon-liked" : "card-icon"} />
                    </IconButton> */}
                    <div className="custom-card-footer">
                        <Link to={'/maiores-informacoes'} className="link-to-novo-projeto">
                            <span>SABER MAIS</span>
                        </Link>
                        <IconButton aria-label="Share">
                            <ShareIcon className="card-icon" />
                        </IconButton>
                    </div>

                </CardActions>
            </Card>
        )
    }
}