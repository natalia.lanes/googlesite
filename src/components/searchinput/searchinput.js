import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import './searchinput.css'
const styles = {
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
        maxHeight:'45px',
    },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        width: 1,
        height: 28,
        margin: 4,
    },
};
class SearchInput extends Component {

    render() {
        const { classes } = this.props;
        return (
            <Paper className={classes.root} elevation={1}>
                <IconButton className={classes.iconButton}>
                    <div className="google-image">
                        <img className="google-image" src="https://ssl.gstatic.com/images/branding/googleg/2x/googleg_standard_color_64dp.png" />
                    </div>
                </IconButton>
                <Divider className={classes.divider} />
                <InputBase className={classes.input} placeholder="O que você procura?" />
                <Divider className={classes.divider} />
                <IconButton className={classes.iconButton} aria-label="Search">
                    <SearchIcon />
                </IconButton>
                {/* <Divider className={classes.divider} />
                <IconButton color="primary" className={classes.iconButton} aria-label="Directions">
                    <DirectionsIcon />
                </IconButton> */}
            </Paper>
        );
    }
}

SearchInput.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchInput);