import React, { Component } from 'react';
import './sidebar.css'
import sidebar from '../../ui/assets/images/sidebar.svg'
import youtube from '../../ui/assets/images/youtube-icon.svg'
import chalkboard from '../../ui/assets/images/chalkboard-icon.svg'
import employees from '../../ui/assets/images/skills.svg'
import { Link } from 'react-router-dom'


export default class SideBar extends Component {

    constructor() {
        super()
        this.state = {
            isTagSelected: "false",
            tags: [{ content: '#youtube', notSelected: true }, { content: "#cardboards", notSelected: true },
            { content: "#formularios", notSelected: true }, { content: "#realidadevirtual", notSelected: true }],
        }
    }

    onTagSelect = (i) => {
        let oldArray = this.state.tags
        oldArray[i].notSelected = !oldArray[i].notSelected
        this.setState({ tags: oldArray })
        this.onTagSelectChangeClass(i)
    }

    onTagSelectChangeClass = (i) => {
        return this.state.tags[i].notSelected ? "hashtag-filtro" : "hashtag-filtro-selected"
    }

    redirectToYoutube = () => {
        window.location.replace('https://www.youtube.com.br')
    }

    render() {
        return (
            <div className={this.props.selected ? "sidebar" : "sidebar-none"}>
                <div className="unilasalle-logo-sidebar">
                    <img src={sidebar}></img>
                </div>
                <div className="sidebar-title-div">
                    <h1 className="sidebar-title">Encontre projetos baseados no seu interesse</h1>
                </div>
                <div className="sidebar-options">
                    <div className="nao-filtrar-div">
                        <h1 className="nao-filtrar-h1">Não filtrar</h1>
                    </div>
                    {this.state.tags.map((tag, i) => {
                        return (
                            <div className={this.onTagSelectChangeClass(i)} key={i}>
                                <h2 onClick={() => this.onTagSelect(i)}> {tag.content}</h2>
                            </div>
                        )
                    })}
                </div>
                <div className="youtube">
                    <div className="comecar-a-inovar-div">
                        <div className="imagem-info">
                            <img src={chalkboard}></img>
                        </div>
                        <div className="texto-info">
                            <Link to={'/sugerir-novo-projeto'} className="link-to-informacoes">
                                <span>Começar a inovar</span>
                            </Link>
                        </div>
                    </div>
                    <div className="conhecer-o-canal-div" onClick={this.redirectToYoutube}>
                        <div className="imagem-info">
                            <img src={youtube}></img>
                        </div>
                        <div className="texto-info">
                            <span>Conhecer o canal</span>
                        </div>
                    </div>
                    <div className="informacoes-colaboradores-div">
                        <div className="imagem-info">
                            <img src={employees}></img>
                        </div>
                        <div className="texto-info" onClick={this.redirectToInformacoes}>
                            <Link to={'/informacoes'} className="link-to-informacoes">
                                <span className="span">Informações para colaboradores</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}