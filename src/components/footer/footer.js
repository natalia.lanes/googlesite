import React, { Component, Fragment } from 'react';
import './footer.css'
import GoogleLogo from '../../ui/assets/images/google-bw-icon.png'

export default class Footer extends Component {


    redirectToUnilasalle = () => {
        window.location.replace('https://www.unilasalle.edu.br/canoas')
    }
    render() {
        return (
            <Fragment>
                <div className="footer">
                    <div className="google-bw-logo">
                        <img src={GoogleLogo} />
                    </div>
                    <div className="about">
                        <button onClick={this.redirectToUnilasalle} className="about-button">Sobre o UniLaSalle</button>
                    </div>
                </div>
            </Fragment>
        )
    }
}