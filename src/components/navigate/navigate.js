import React, { Component } from 'react';
import './navigate.css'
import { Link } from 'react-router-dom'

export default class Navigate extends Component {

    render() {
        return (
            <div className="navigate-div">
                {/* <div className="navigate-span-title">
                    <span className="title-span">Projetos em andamento</span>
                </div> */}
                <div className="navigate-span-options">
                    <Link to={'/'} className="link-to-informacoes">
                        <span className="options-span">Home</span>
                    </Link>
                </div>
            </div>)
    }
}