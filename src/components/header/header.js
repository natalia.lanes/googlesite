import React, { Component, Fragment } from 'react';
import './header.css'
import GoogleLogo from '../../ui/assets/images/google-icon.png'
import BurguerMenu from '../../ui/assets/images/burguer-menu.svg'
import ButtonHome from '../../ui/assets/images/home-icon.svg'
import { Link } from 'react-router-dom'

export default class Header extends Component {

    goToYoutube = () => {
        window.location.replace('https://www.youtube.com')
    }
    render() {
        return (
            <Fragment>
                <div className="header">
                    <div className="burguer-menu" onClick={this.props.onClick}>
                        <img src={BurguerMenu} />
                    </div>
                    <div className="home-button">
                        <Link to={'/'}>
                            <img src={ButtonHome}></img>
                        </Link>
                    </div>
                    <div className="google-logo">
                        <img src={GoogleLogo} />
                    </div>
                    <div className="login">
                        <button className="login-button">FAZER LOGIN</button>
                    </div>
                </div>
            </Fragment>
        )
    }
}