import React, { Component } from 'react';
import xperience from '../../ui/assets/images/xperience2.jpg'
import { Link } from 'react-router-dom'
import './subheader.css'

export default class SubHeader extends Component {

    render() {
        return (
            <div className="carousel">
                <div className="carousel-content">
                    <img src={xperience} />
                    <div className="title-car">
                        <h1 className="carousel-title">{this.props.subheaderContent}</h1>
                        <div className="carousel-title-h2-div">
                            <h2 className="carousel-title-h2">{this.props.subheaderH2}</h2>
                            <Link to={'/sugerir-novo-projeto'} className="link-to-novo-projeto">
                                <h3 className="carousel-title-h3">{this.props.subheaderH3}</h3>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}