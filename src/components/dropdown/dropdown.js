import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
});

class DropDown extends React.Component {
    state = {
        expanded: null,
    };

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    render() {
        const { classes } = this.props;
        const { expanded } = this.state;

        return (
            <div className={classes.root}>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Ponto</Typography>
                        {/* <Typography className={classes.secondaryHeading}>
                            You are currently not an owner
            </Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            A Universidade não possui banco de horas.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Benefícios</Typography>
                        {/* <Typography className={classes.secondaryHeading}>
                            Vale refeição, vale transporte e desconto na graduação.
            </Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            Vale refeição, vale transporte e desconto na graduação.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChange('panel3')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Como reservar uma sala?</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            Você pode ir até o setor de suporte localizado próximo ao prédio 8.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel4'} onChange={this.handleChange('panel4')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Ponto</Typography>
                        {/* <Typography className={classes.secondaryHeading}>
                            You are currently not an owner
            </Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            A Universidade não possui banco de horas.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel5'} onChange={this.handleChange('panel5')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Ponto</Typography>
                        {/* <Typography className={classes.secondaryHeading}>
                            You are currently not an owner
            </Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            A Universidade não possui banco de horas.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel6'} onChange={this.handleChange('panel6')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Ponto</Typography>
                        {/* <Typography className={classes.secondaryHeading}>
                            You are currently not an owner
            </Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            A Universidade não possui banco de horas.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel7'} onChange={this.handleChange('panel7')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography className={classes.heading}>Ponto</Typography>
                        {/* <Typography className={classes.secondaryHeading}>
                            You are currently not an owner
            </Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            A Universidade não possui banco de horas.
            </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

DropDown.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DropDown);
