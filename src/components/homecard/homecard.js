import React, { Component } from 'react';
import './homecard.css'
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

export default class HomeCard extends Component {

    constructor() {
        super()
        this.state = {
            isLiked: false
        }
    }

    like = () => {
        this.setState({ isLiked: !this.state.isLiked })
    }

    render() {

        return (

            <Card className={this.props.className}>
                <CardHeader className="card-home-header"
                    avatar={
                        <Avatar className="card-avatar">
                            <img className="" src={this.props.image}></img>
                        </Avatar>
                    }
                    title={this.props.title}
                />
                <CardContent className="card-summary">
                    <Typography component="p" className="card-summary">
                        {this.props.projectDescription}
                    </Typography>
                </CardContent>
                <CardActions disableActionSpacing className="card-buttons">
                        <Button size="small" className="card-summary teste">
                            SABER MAIS
                   </Button>
                </CardActions>
            </Card>
        )
    }
}