import React, { Component } from 'react'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import SubHeader from '../../../components/subheader/subheader'

import './suggestaproject.css'


export default class SuggestAProject extends Component {

    render() {
        return (
            <div>
                <Fab color="primary" aria-label="Add" className="floating-button" onClick={this.addPost}>
                    <AddIcon />
                </Fab>
                <SubHeader subheaderContent="Preencha os campos abaixo para que possamos entrar 
                em contato com você e dar início a construção de uma aula inovadora utilizando as ferramentas Google"></SubHeader>
                <div className="container-suggest">
                    <form className="form-suggest">
                <div className="form-group">
                <input className="input-form" placeholder="    Digite um breve resumo sobre o que pretende fazer em aula"></input>
                <input className="input-form" placeholder="    Digite um breve resumo sobre o que pretende fazer em aula"></input>
                <div className="button-container">
                <button className="enviar-mensagem-button">Enviar Mensagem</button>
                </div>
                </div>                   
                </form>
            </div>
            </div>
        )
    }
}