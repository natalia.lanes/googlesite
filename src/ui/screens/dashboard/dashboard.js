import React, { Component } from 'react';
import './dashboard.css'
import xperience from '../../assets/images/xperience2.jpg'
import CustomCard from '../../../components/customcard/customcard'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import SubHeader from '../../../components/subheader/subheader';

export default class Dashboard extends Component {

    constructor() {
        super()
        this.state = {
        }
    }

    addPost = () => {

    }

    render() {
        return (
            <div>
                <Fab color="primary" aria-label="Add" className="floating-button" onClick={this.addPost}>
                    <AddIcon />
                </Fab>
                {/* <div className="carousel">
                    <div className="carousel-content">
                        <img src={xperience} />
                        <div className="title-car">
                            <h1 className="carousel-title">{this.props.subheaderContent}</h1>
                            <div className="carousel-title-h2-div">
                                <h2 className="carousel-title-h2">{this.props.subheaderH2}</h2>
                                <h3 className="carousel-title-h3">{this.props.subheaderH3}</h3>
                            </div>
                        </div>
                    </div>
                </div> */}
                <SubHeader subheaderContent="Descubra como os professores da Universidade La Salle em parceria com
os bolsistas Google for Education estão utilizando tecnologia para revolucionar
a forma de dar aula!" subheaderH2="Quer participar dessa iniciativa?" subheaderH3="Começar a preparar uma aula interativa"></SubHeader>
                <div className="container">
                    <CustomCard projectName="Arrecadação de doces para a Páscoa"
                        userName="Natália Lanes"
                        projectDescription="Alunos se reunem para montagem de kits arrecadados para doação. O encontro aconteceu na Xperience Room da Universidade." postTime="09 de abril de 2019"></CustomCard>
                    <CustomCard projectName="Arrecadação de doces para a Páscoa"
                        userName="Natália Lanes"
                        projectDescription="Alunos se reunem para montagem de kits arrecadados para doação. O encontro aconteceu na Xperience Room da Universidade." postTime="09 de abril de 2019"></CustomCard>
                    <CustomCard projectName="Arrecadação de doces para a Páscoa"
                        userName="Leonardo Lopes"
                        projectDescription="Alunos se reunem para montagem de kits arrecadados para doação. O encontro aconteceu na Xperience Room da Universidade." postTime="09 de abril de 2019"></CustomCard>
                    <CustomCard projectName="Arrecadação de doces para a Páscoa"
                        userName="Leonardo Lopes"
                        projectDescription="Alunos se reunem para montagem de kits arrecadados para doação. O encontro aconteceu na Xperience Room da Universidade." postTime="09 de abril de 2019"></CustomCard>
                    <CustomCard projectName="Arrecadação de doces para a Páscoa"
                        userName="Natália Lanes"
                        projectDescription="Alunos se reunem para montagem de kits arrecadados para doação. O encontro aconteceu na Xperience Room da Universidade." postTime="09 de abril de 2019"></CustomCard>
                </div>
            </div>
        )
    }
}