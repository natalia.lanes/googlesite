import React, { Component } from 'react'
import HomeCard from '../../../components/homecard/homecard'
import lasalleIcon from '../../assets/images/lasalle-icon.png'
import peopleIcon from '../../assets/images/people-icon.svg'
import searchIcon from '../../assets/images/search-icon.svg'
import goodPracticesIcon from '../../assets/images/good-practices-icon.svg'
import SubHeader from '../../../components/subheader/subheader'

import './homepage.css'
import CustomCard from '../../../components/customcard/customcard';

export default class HomePage extends Component {

    render() {
        return (
            <div className="container-home-page">
                {/* <div className="subheader-home"> */}
                    {/* <div className="subheader-home-image">
                        <img src={lasalleIcon} />
                    </div>
                    <div className="text-content">
                        <h1 className="subheader-home-title">Conheça, participe e sugira projetos para a Universidade LaSalle</h1>
                    </div> */}
                    <SubHeader subheaderContent="Descubra como os professores da Universidade La Salle em parceria com
os bolsistas Google for Education estão utilizando tecnologia para revolucionar
a forma de dar aula!" subheaderH2="Quer participar dessa iniciativa?" subheaderH3="Começar a preparar uma aula interativa"></SubHeader>
                {/* </div> */}
                <div className="container-home">
                    {/* <HomeCard image={searchIcon} title="PESQUISA" className="card-search"
                        projectDescription="Alunos se reunem para montagem de kits arrecadados para doação. O encontro aconteceu na Xperience Room da Universidade." postTime="09 de abril de 2019"></HomeCard>
                    <HomeCard image={peopleIcon} title="EXTENSÃO" className="card-extension"
                        projectDescription="Projetos que tem como objetivo ampliar a atuação da universidade com a comunidade externa. Auxiliando o público geral a adquirir o conhecimento gerado pelos projetos de pesquisa e ensino." postTime="09 de abril de 2019"></HomeCard>
                    <HomeCard image={goodPracticesIcon} title="BOAS PRÁTICAS" className="card-good-pratices"
                        projectDescription="Aulas revolucionárias realizadas por professores, com o auxílio de bolsistas, para inovar a forma como o conhecimento é gerado em sala de aula. São utilizados diversas tecnologias Google para tornar os alunos mais ativos e interessados nos conteúdos." postTime="09 de abril de 2019"></HomeCard> */}
                    <CustomCard projectName="Professores utilizam ferramentas Google em sala de aula" projectDescription="Com auxílio de bolsistas Google for Education, professores da universidade La Salle utilizam tecnologia Google para realização de aulas interativas."></CustomCard>
                    <CustomCard projectName="Professores utilizam ferramentas Google em sala de aula" projectDescription="Com auxílio de bolsistas Google for Education, professores da universidade La Salle utilizam tecnologia Google para realização de aulas interativas."></CustomCard>
                    <CustomCard projectName="Professores utilizam ferramentas Google em sala de aula" projectDescription="Com auxílio de bolsistas Google for Education, professores da universidade La Salle utilizam tecnologia Google para realização de aulas interativas."></CustomCard>
                </div>
            </div>
        )
    }
}