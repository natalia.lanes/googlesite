import React, { Component } from 'react';
import './employessinformation.css'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import Dropdown from '../../../components/dropdown/dropdown';
import SearchInput from '../../../components/searchinput/searchinput';
import SubHeader from '../../../components/subheader/subheader';
import Navigate from './../../../components/navigate/navigate'

export default class EmployeesInformation extends Component {

    constructor() {
        super()
        this.state = {
        }
    }

    addPost = () => {

    }

    render() {
        return (
            <div>
                <Fab color="primary" aria-label="Add" className="floating-button" onClick={this.addPost}>
                    <AddIcon />
                </Fab>
                {/* <Navigate></Navigate> */}
                <SubHeader subheaderContent="Descubra como os professores da Universidade La Salle em parceria com
os bolsistas Google for Education estão utilizando tecnologia para revolucionar
a forma de dar aula!" subheaderH2="Quer participar dessa iniciativa?" subheaderH3="Começar a preparar uma aula interativa"></SubHeader>
                <div className="container-employees">
                    <div className="container-header">
                        <SearchInput></SearchInput>
                    </div>
                    <Dropdown></Dropdown>
                </div>
            </div>
        )
    }
}